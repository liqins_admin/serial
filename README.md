# serial
Java串口传输数据例子

个人博客: https://www.cnblogs.com/sailing-2022-04/articles/16140481.html

#项目介绍


最近笔者接触到串口编程，网上搜了些资料，顺便整理一下。网上都在推荐使用Java RXTX开源类库，它提供了Windows、Linux等不同操作系统下的串口和并口通信实现，遵循GNU LGPL协议。看起来不错，写个例子试试。

#准备运行环境

下载RXTX
RXTX下载地址是：http://fizzed.com/oss/rxtx-for-java， 笔者操作系统是Windows10，下载对应版本的压缩包，解压后复制RXTXcomm.jar到D:\Program Files\Java\jdk1.8.0_152\jre\lib\ext目录下； 复制rxtxParallel.dll和rxtxSerial.dll到D:\Program Files\Java\jdk1.8.0_152\jre\bin目录下。

注意：安装jdk时可能也顺便装了jre，需要复制到jdk的jre目录下。

下载Virtual Serial Port Driver
Virtual Serial Port Driver是一款非常好用的虚拟串口模拟软件，可以在计算机模拟串口，方便开发和测试。安装后打开界面如下：


可以看到右侧默认出现COM1和COM2的串口，点击Add pair就可以创建这两个串口了，打开计算机管理，可以看到本机多了这两个端口，如下图所示：



#创建项目
创建serial项目，如下图所示：



#串口工具类
现在可以写一个串口工具类，方便开发和测试，代码如下：

                                
public class SerialPortUtils {

    private static Logger log = LoggerFactory.getLogger(SerialPortUtils.class);

    /**
        * 打卡串口
        * @param portName 串口名
        * @param baudRate 波特率
        * @param dataBits 数据位
        * @param stopBits 停止位
        * @param parity 校验位
        * @return 串口对象
        */
    public static SerialPort open(String portName, Integer baudRate, Integer dataBits,
                                        Integer stopBits, Integer parity) {
        SerialPort result = null;
        try {
            // 通过端口名识别端口
            CommPortIdentifier identifier = CommPortIdentifier.getPortIdentifier(portName);
            // 打开端口，并给端口名字和一个timeout（打开操作的超时时间）
            CommPort commPort = identifier.open(portName, 2000);
            // 判断是不是串口
            if (commPort instanceof SerialPort) {
                result = (SerialPort) commPort;
                // 设置一下串口的波特率等参数
                result.setSerialPortParams(baudRate, dataBits, stopBits, parity);
                log.info("打开串口{}成功", portName);
            }else{
                log.info("{}不是串口", portName);
            }
        } catch (Exception e) {
            log.error("打开串口{}错误", portName, e);
        }
        return result;
    }

    /**
        * 串口增加数据可用监听器
        * @param serialPort
        * @param listener
        */
    public static void addListener(SerialPort serialPort, DataAvailableListener listener) {
        if(serialPort == null){
            return;
        }
        try {
            // 给串口添加监听器
            serialPort.addEventListener(new SerialPortListener(listener));
            // 设置当有数据到达时唤醒监听接收线程
            serialPort.notifyOnDataAvailable(Boolean.TRUE);
            // 设置当通信中断时唤醒中断线程
            serialPort.notifyOnBreakInterrupt(Boolean.TRUE);
        } catch (TooManyListenersException e) {
            log.error("串口{}增加数据可用监听器错误", serialPort.getName(), e);
        }
    }

    /**
        * 从串口读取数据
        * @param serialPort
        * @return
        */
    public static byte[] read(SerialPort serialPort) {
        byte[] result = {};
        if(serialPort == null){
            return result;
        }
        InputStream inputStream = null;
        try {
            inputStream = serialPort.getInputStream();

            // 缓冲区大小为1个字节，可根据实际需求修改
            byte[] readBuffer = new byte[7];

            int bytesNum = inputStream.read(readBuffer);
            while (bytesNum > 0) {
                result = ArrayUtil.addAll(result, readBuffer);
                bytesNum = inputStream.read(readBuffer);
            }
        } catch (IOException e) {
            log.error("串口{}读取数据错误", serialPort.getName(), e);
        } finally {
            IoUtil.close(inputStream);
        }
        return result;
    }

    /**
        * 往串口发送数据
        * @param serialPort
        * @param data
        */
    public static void write(SerialPort serialPort, byte[] data) {
        if(serialPort == null){
            return;
        }
        OutputStream outputStream = null;
        try {
            outputStream = serialPort.getOutputStream();
            outputStream.write(data);
            outputStream.flush();
        } catch (Exception e) {
            log.error("串口{}发送数据错误", serialPort.getName(), e);
        } finally {
            IoUtil.close(outputStream);
        }
    }

    /**
        * 关闭串口
        * @param serialPort
        */
    public static void close(SerialPort serialPort) {
        if (serialPort != null) {
            serialPort.close();
            log.warn("串口{}关闭", serialPort.getName());
        }
    }

    /**
        * 查询可用端口
        * @return 串口名List
        */
    public static List listPortName() {
        List result = new ArrayList<>();

        // 获得当前所有可用端口
        Enumeration serialPorts = CommPortIdentifier.getPortIdentifiers();
        if(serialPorts == null){
            return result;
        }

        // 将可用端口名添加到List并返回该List
        while (serialPorts.hasMoreElements()) {
            result.add(serialPorts.nextElement().getName());
        }
        return result;
    }
}
                                
                            

#测试代码
测试代码如下。

                                
public class SerialPortTest {

    public static void main(String[] args) throws Exception{

        // 打开客户端串口
        SerialPort serialPort = SerialPortUtils.open("COM1", 115200, SerialPort.DATABITS_8,
               SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        
        // 打开服务端串口
        SerialPort serialPort2 = SerialPortUtils.open("COM2", 115200, SerialPort.DATABITS_8,
               SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        
        // 监听客户端端口读取数据
        SerialPortUtils.addListener(serialPort, () -> {
           byte[] data = SerialPortUtils.read(serialPort);
           System.out.println("读取数据:"+HexUtil.encodeHexStr(data));
        });
        
        // 客户端往服务端串口发送数据
        byte[] data = {0x01, 0x02, 0x03};
        SerialPortUtils.write(serialPort, data);
        
        // 服务端监听端口读取数据
        SerialPortUtils.addListener(serialPort2, () -> {
           byte[] data2 = SerialPortUtils.read(serialPort2);
           System.out.println("读取数据:"+HexUtil.encodeHexStr(data2));
        });

        /*// 关闭串口
        Thread.sleep(2000);
        SerialPortUtils.close(serialPort);*/

        // 测试可用端口
        //SerialPortUtils.listPortName().forEach(o -> System.out.println(o));
    }
}
                                
                            


运行测试代码后，idea控制台也会显示收到010203，说明COM1和COM2串口互相发送和接收数据成功。