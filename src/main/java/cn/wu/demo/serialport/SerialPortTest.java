package cn.wu.demo.serialport;

import cn.hutool.core.util.HexUtil;
import cn.wu.demo.serialport.util.SerialPortUtils;
import gnu.io.SerialPort;

/**
 * 串口测试程序
 * @author Sailing
 * @date 2022/4/13
 */
public class SerialPortTest {

    public static void main(String[] args) throws Exception{

        // 打开客户端串口
        SerialPort serialPort = SerialPortUtils.open("COM1", 921600, SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        // 打开服务端串口
        SerialPort serialPort2 = SerialPortUtils.open("COM2", 921600, SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

        // 监听客户端端口读取数据
       /* SerialPortUtils.addListener(serialPort, () -> {
            byte[] data = SerialPortUtils.read(serialPort);
            System.out.println("客户端读取数据:"+HexUtil.encodeHexStr(data));
        });*/

        // 客户端往服务端串口发送数据
        //  byte[] data = {0x01, 0x02, 0x03};
        byte[] data = {(byte) 0xaa, (byte) 0xbb, 0x09, 0x0f, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14};
        SerialPortUtils.write(serialPort, data);


        // 服务端监听端口读取数据
        SerialPortUtils.addListener(serialPort2, () -> {
            byte[] data2 = SerialPortUtils.read(serialPort2);
            System.out.println("服务端获取数据:"+HexUtil.encodeHexStr(data2));
        });





        /*// 关闭串口
        Thread.sleep(2000);
        SerialPortUtils.close(serialPort);*/

        // 测试可用端口
       // SerialPortUtils.listPortName().forEach(o -> System.out.println("测试可用端口"+o));

    }
}
